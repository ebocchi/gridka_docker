### Welcome to the GridKa Docker Tutorial

This repository contains various Dockerfiles, docker-compose files, examples, and configuration files to build and run containers.


Please refer to the following subfolders:

 * docker -- for Dockerfiles and exercises with Docker;
 * docker-compose -- for yaml descriptors and extended examples with docker-compose.

