# Use the official ubuntu image as parent image
FROM ubuntu:latest

# Define who is responsible for this Dockerifle
MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>

# Install Apache HTTP Server
RUN apt-get update && apt-get -y install cowsay

# cowsay in installed at '/usr/games/', let's add it to the PATH
ENV PATH="/usr/games/:${PATH}"

# Copy the wrapper for cowsay
ADD cowsay7.d/start_cowsay.sh /root/start_cowsay.sh

# Define environment variables
ENV WAITFILE_SENTENCE="I don't know what to say..."
ENV BAILOUT_SENTENCE="Too late. See you next time!"
ENV FNAME="/root/sentences.log"
ENV SLEEP="10"

# Use start_cowsay.sh as the command to run when starting the container
CMD ["/bin/bash", "/root/start_cowsay.sh"]
