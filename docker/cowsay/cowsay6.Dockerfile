# Use the official ubuntu image as parent image
FROM ubuntu:latest

# Define who is responsible for this Dockerifle
MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>

# Install Apache HTTP Server
RUN apt-get update && apt-get -y install cowsay

# cowsay in installed at '/usr/games/', let's add it to the PATH
ENV PATH="/usr/games/:${PATH}"

# Define a default sentence said by the cow
ENV SENTENCE="Hello Gridka People!"

# Define the executable to run when starting the container
CMD ["/bin/bash", "-c", "/usr/games/cowsay $SENTENCE"]
