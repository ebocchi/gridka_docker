#! /bin/bash

# Bail out if the repository path is not specified
if [[ x"$REPO" == x"" ]];
then
  echo "ERROR: Git repository not defined"
  exit 1
fi

# Pull updates from the repo every 60 seconds
while true;
do
  sleep 60
  echo `date '+%Y-%m-%d %H:%M:%S'`" -- Pulling updates from Git repository..."
  cd /webdata && git pull
done
