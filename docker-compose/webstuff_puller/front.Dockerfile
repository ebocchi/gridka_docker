# Use the official centos image as parent image
FROM centos:latest

# Define who is responsible for this Dockerifle
MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>

# Install Apache HTTP Server
RUN yum -y install httpd

# Add the configuration file for Apache
ADD front.d/upstream.conf /etc/httpd/conf.d/upstream.conf

# Tell the container to listed to port 80 at runtime
EXPOSE 80/tcp

# Define the executable to run when starting the container
CMD ["httpd", "-DFOREGROUND"]
