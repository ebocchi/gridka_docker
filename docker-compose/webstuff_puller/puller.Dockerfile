# Use the official centos image as parent image
FROM centos:latest

# Define who is responsible for this Dockerifle
MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>

# Install Git
RUN yum -y install git

# Clone the repository with webdata
RUN git clone https://gitlab.cern.ch/ebocchi/webpage_repo.git /webdata

# Add the puller script
ADD puller.d/puller.sh /root/puller.sh

# Define the executable to run when starting the container
CMD ["/bin/bash", "/root/puller.sh"]
